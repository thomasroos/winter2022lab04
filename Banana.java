public class Banana {
	private int length;
	private String color;
	private String texture;
	
	public Banana(int length,String color,String texture) {
		this.length = length;
		this.color = color;
		this.texture = texture;
	}
	
	public int getLength() {
		return this.length;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public String getTexture() {
		return this.texture;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public void setTexture(String texture) {
		this.texture = texture;
	}
	
	public void printInformation(String texture) {
		System.out.print(texture);
	}
}
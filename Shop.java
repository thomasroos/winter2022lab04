import java.util.*;
public class Shop {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Banana[] banana = new Banana[4];
		
		for (int i=0; i < 4; i++) {
			System.out.println("--Banana #" + (i+1) + "--");
			
			System.out.println("Banana Length: ");
			String o = scan.nextLine();
			int x = Integer.parseInt(o);
			
			System.out.println("Banana Color: ");
			String y = scan.nextLine();
			
			System.out.println("Banana Texture: ");
			String z = scan.nextLine();
			
			banana[i] = new Banana(x,y,z);
		}
		
		System.out.println("Banana Color: ");
		String x = scan.nextLine();
		System.out.println("Banana Texture: ");
		String y = scan.nextLine();
		
		banana[3].setColor(x);
		banana[3].setTexture(y);
		
		System.out.println("Length: " + banana[3].getLength());
		System.out.println("Color: " + banana[3].getColor());
		System.out.println("Texture: " + banana[3].getTexture());
	}
}